package com.example.lv1_dz_halloffamejanadukic;

import androidx.appcompat.app.AppCompatActivity;

import android.nfc.Tag;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnInspiration;
    Button btnEditDescription;
    Button btnReset;
    RadioButton rbFirstPerson;
    RadioButton rbSecondPerson;
    RadioButton rbThirdPerson;
    ImageView ivMarieCurie;
    ImageView ivDavidDukic;
    ImageView ivAlbertEinstein;
    EditText etEnterNewDescription;

    TextView tvMCDescription;
    TextView tvDDDescription;
    TextView tvAEDescription;
    RadioGroup radioGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUI();
    }

    private void initializeUI() {
        this.btnInspiration = (Button) findViewById(R.id.btnInspiration);
        this.btnEditDescription = (Button) findViewById(R.id.btnEditDescription);
        this.ivMarieCurie = (ImageView) findViewById(R.id.ivMarieCurie);
        this.ivDavidDukic = (ImageView) findViewById(R.id.ivDavidDukic);
        this.ivAlbertEinstein = (ImageView) findViewById(R.id.ivAlbertEinstein);
        this.etEnterNewDescription = (EditText) findViewById(R.id.etEnterNewDescription);
        this.btnReset = (Button) findViewById(R.id.btnReset);

        this.rbFirstPerson = findViewById(R.id.rbFirstPerson);
        this.rbSecondPerson = findViewById(R.id.rbSecondPerson);
        this.rbThirdPerson = findViewById(R.id.rbThirdPerosn);
        this.radioGroup = findViewById(R.id.rgOdabiriOsoba);

        this.tvMCDescription = findViewById(R.id.tvInformationMarieCurie);
        this.tvDDDescription = findViewById(R.id.tvInformationDavidDukic);
        this.tvAEDescription = findViewById(R.id.tvInformationAlbertEinstein);

        this.btnInspiration.setOnClickListener(this);
        this.ivMarieCurie.setOnClickListener(this);
        this.ivDavidDukic.setOnClickListener(this);
        this.ivAlbertEinstein.setOnClickListener(this);
        this.btnEditDescription.setOnClickListener(this);
        this.btnReset.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == this.btnInspiration) {
            Random random = new Random();
            int randomNumber = random.nextInt(2 - 0 + 1) + 0;
            String[] InspirationArray = new String[]{getString(R.string.MCInterestingFact), getString(R.string.DDInterestingFact), getString(R.string.AEInterestingFact)};
            displayToast(randomNumber, InspirationArray[randomNumber]);
            Log.wtf("TAG", "Something happened.");
        } else if (v == this.ivMarieCurie) {
            this.ivMarieCurie.setVisibility(View.INVISIBLE);
        } else if (v == this.ivDavidDukic) {
            this.ivDavidDukic.setVisibility(View.INVISIBLE);
        } else if (v == this.ivAlbertEinstein) {
            this.ivAlbertEinstein.setVisibility(View.INVISIBLE);
        } else if (v == this.btnEditDescription) {
            String stringFromEditText = this.etEnterNewDescription.getText().toString();
            if (this.rbFirstPerson.isChecked()) {
                this.tvMCDescription.setText(stringFromEditText);
            } else if (this.rbSecondPerson.isChecked()) {
                this.tvDDDescription.setText(stringFromEditText);
            } else if (this.rbThirdPerson.isChecked()) {
                this.tvAEDescription.setText(stringFromEditText);
            }
        } else if (v == this.btnReset) {
            this.ivMarieCurie.setVisibility(View.VISIBLE);
            this.ivAlbertEinstein.setVisibility(View.VISIBLE);
            this.ivDavidDukic.setVisibility(View.VISIBLE);
            this.tvMCDescription.setText(getString(R.string.textviewInformationMarieCurie));
            this.tvAEDescription.setText(getString(R.string.textviewInformationAlbertEinstein));
            this.tvDDDescription.setText(getString(R.string.textviewInformationDavidDukic));
            this.etEnterNewDescription.setText("");
            this.etEnterNewDescription.setHint(getString(R.string.edittextEnterNewDescription));
            this.radioGroup.clearCheck();
        }
    }

    private void displayToast(int number, String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

}
