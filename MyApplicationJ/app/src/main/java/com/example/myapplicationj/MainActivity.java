package com.example.myapplicationj;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tvShoesSize;
    Button btnCalculateShoeSize;
    EditText etEnterShoeSize;
    ImageView ivShoes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUI();
    }

    private void initializeUI(){
        this.tvShoesSize = (TextView) findViewById(R.id.tvShoeSize);
        this.btnCalculateShoeSize = (Button) findViewById((R.id.btnCalculateShoeSize));
        this.etEnterShoeSize = (EditText) findViewById(R.id.etEnterShoeSize);
        this.ivShoes = (ImageView) findViewById((R.id.deltaRelative));

        this.btnCalculateShoeSize.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int ShoeSize;
        ShoeSize = Integer.parseInt(etEnterShoeSize.getText().toString());
        ShoeSize = ShoeSize/5;
        this.tvShoesSize.setText("US shoe size is: " + ShoeSize);
        Log.d("Čvarci","Rezanje čvaraka");
        displayToast();
        Log.wtf("Čvarci","Čipičips od gice");
    }

    private void displayToast(){
        Toast.makeText(this,"čvarci se peku",Toast.LENGTH_LONG).show();

    }
}